package Assignments.QuestionFour;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author hanson kibet
 */
class TriangleTest {

    @Test
    void solveTriangle() {
        assertEquals(0,new Triangle().solveTriangle(0));
        assertEquals(1,new Triangle().solveTriangle(1));
        assertEquals(3,new Triangle().solveTriangle(2));
    }
}