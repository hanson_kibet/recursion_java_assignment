package Assignments.QuestionFour;

/**
 * @author hanson kibet
 */
public class Triangle {
    public int solveTriangle(int numRows){
//        numRows > 0 is the base condition of which whenever it is false it returns 0
        return (numRows > 0)?numRows + solveTriangle(numRows-1) : 0;
    }
}
