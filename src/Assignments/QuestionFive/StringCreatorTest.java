package Assignments.QuestionFive;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author hanson kibet
 */
class StringCreatorTest {

    @Test
    void parentBit() {
        assertEquals("(abc)",new StringCreator().ParentBit("xyz(abc)123"));
        assertEquals("(hello)",new StringCreator().ParentBit("x(hello)"));
        assertEquals("(xy)",new StringCreator().ParentBit("(xy)1"));
    }
}