package Assignments.QuestionFive;

/**
 * @author hanson kibet
 */
public class StringCreator {
    public String ParentBit(String b){
        if(b.isEmpty()){ // base condition
            return b;
        }
        return (b.charAt(0) == '(')?b.charAt(0)+b.substring(1,b.indexOf(')')+1): ParentBit(b.substring(1));
    }
}
