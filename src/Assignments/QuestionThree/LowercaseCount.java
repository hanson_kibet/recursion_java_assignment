package Assignments.QuestionThree;

/**
 * @author hanson kibet
 */
public class LowercaseCount {
    public int xCount(String a){
        int count;
        if (a.length() == 0){ // base condition
            return 0;
        }
        count = (a.charAt(0) == 'x')?1 : 0; // checking if first index element is equals to x
        return count + xCount(a.substring(1));
    }
}
