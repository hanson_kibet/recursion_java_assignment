package Assignments.QuestionThree;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author hanson kibet
 */
class LowercaseCountTest {

    @Test
    void xCount() {
        assertEquals(4,new LowercaseCount().xCount("xxhixx"));
        assertEquals(3,new LowercaseCount().xCount("xhixhix"));
        assertEquals(0,new LowercaseCount().xCount("hi"));
    }
}